// CON UN SWITCH

let numeros = new Array(3);

let numRepetido = "No hay numeros repetidos";

for (let i = 0; i < numeros.length; i++) {
    numeros[i] = +prompt("Introduce número");
}

switch (true) {
    case numeros[0] == numeros[1] && numeros[0] == numeros[2]:
        numRepetido = "Los tres números son iguales";
        break;
    case numeros[0] == numeros[1]:
    case numeros[0] == numeros[2]:
        numRepetido = numeros[0];
        break;
    case numeros[1] == numeros[2]:
        numRepetido = numeros[1];
        break;

    default:
        break;
}

document.querySelector('.n1').innerHTML = "Número 1 introducido: " + numeros[0];
document.querySelector('.n2').innerHTML = "Número 2 introducido: " + numeros[1];
document.querySelector('.n3').innerHTML = "Número 3 introducido: " + numeros[2];
document.querySelector('.resultado').innerHTML = "Número repetido: " + numRepetido;

