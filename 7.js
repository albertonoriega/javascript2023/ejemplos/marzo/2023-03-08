/**
 * Función que te indica si un texto acaba en S o M
 * @param {*} entrada 
 * @returns si acaba en S o M
 */
function terminar(entrada) {

    let acaba = " NO acaba en S o M";

    if (entrada.toLowerCase().endsWith("m") || entrada.toLowerCase().endsWith("s")) {
        acaba = "El texto termina en S o M"
    }
    return acaba;
}

/**
 * Función que te devuelve la última letra del texto pasado por parámetro
 * @param {*} entrada 
 * @returns ultima letra del texto
 */
function ultimaLetra(entrada) {
    let ultima = entrada.slice(-1).toUpperCase();
    return ultima;
}

// Pedimos un texto al usuario
let texto = prompt("Introduce un texto");

// Llamamos a las funciones
let ultima = ultimaLetra(texto);
let acaba = terminar(texto);

// Modificamos el HTML con las variables calculadas
document.querySelectorAll('span')[0].innerHTML += texto;
document.querySelectorAll('span')[1].innerHTML += ultima;
document.querySelectorAll('div')[2].innerHTML += acaba;

