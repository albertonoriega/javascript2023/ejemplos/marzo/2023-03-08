let notapromedio = 0;

const costeInscripción = 3000;

let pago = 0;
let descuento = 0;

// pedimos la nota media al usuario
notapromedio = +(prompt("Introduce tu nota media"));

// Si la nota es 9 o mas realizamos un descuento
if (notapromedio >= 9) {
    descuento = costeInscripción * 0.2;
}
pago = costeInscripción - descuento;

// Imprimimos el pago a realizar solo si hay descuento
document.write("Cantidad a pagar: " + pago);
