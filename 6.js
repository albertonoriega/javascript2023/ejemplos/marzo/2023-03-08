/**
 * Función que te calcula el número de carácteres de un texto
 * @param {*} entrada Texto escrito por el usuario
 * @returns número de carácteres del texto pasado por parámetro
 */
function longitud(entrada) {
    let salida = entrada.length;
    return salida;
}

let texto = "";

// pedimos un texto al usuario
texto = prompt("Introduce un texto");

// Guardamos en la variable longitudTexto lo que devuelve la funcion longitud
let longitudTexto = longitud(texto);

// Modificamos el HTML
document.querySelectorAll('div')[0].innerHTML += texto;
document.querySelector('span').innerHTML += longitudTexto;