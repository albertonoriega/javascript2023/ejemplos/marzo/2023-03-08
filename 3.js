let numeros = new Array(3);

let numRepetido = "No hay numeros repetidos";

for (let i = 0; i < numeros.length; i++) {
    numeros[i] = +prompt("Introduce número");
}

if (numeros[0] == numeros[1] && numeros[1] == numeros[2]) {
    numRepetido = "Los tres números son iguales";
} else if (numeros[1] == numeros[2]) {
    numRepetido = numeros[1];
} else if (numeros[0] == numeros[1] || numeros[0] == numeros[2]) {
    numRepetido = numeros[0];
}

document.querySelector('.n1').innerHTML = "Número 1 introducido: " + numeros[0];
document.querySelector('.n2').innerHTML = "Número 2 introducido: " + numeros[1];
document.querySelector('.n3').innerHTML = "Número 3 introducido: " + numeros[2];
document.querySelector('.resultado').innerHTML = "Número repetido: " + numRepetido;

